.build_template:
  stage: build
  image: docker
  services:
    - docker:dind
  script:
    - create_service_key
    - docker_login
    - docker_build
    - docker_push

.deploy_template:
  stage: deploy
  image: google/cloud-sdk
  script:
    - create_service_key
    - auth_google_cloud
    - set_service_environment_name
    - create_cloud_run_service
    - make_cloud_run_externally_available
    - make_custom_domain

.deploy_template_review:
  extends: .deploy_template
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    on_stop: stop_review
  when: manual
  only:
    refs:
      - /^feature\/.*$/

.deploy_template_review_cleanup:
  stage: cleanup
  image: google/cloud-sdk
  script:
    - create_service_key
    - auth_google_cloud
    - set_service_environment_name
    - delete_cloud_run_service
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
  when: manual
  only:
    refs:
      - /^feature\/.*$/

.deploy_template_staging:
  extends: .deploy_template
  environment:
    name: staging
    url: https://staging-$SERVICE_NAME.springboardvr.com
  only:
    refs:
      - master

.deploy_template_production:
  extends: .deploy_template
  environment:
    name: production
    url: https://$SERVICE_NAME.springboardvr.com
  when: manual
  only:
    refs:
      - master
      - /^hotfix\/.*$/

.common_functions: &common_functions |
  [[ "$TRACE" ]] && set -x

  function create_service_key () {
    echo $GOOGLE_SA_KEY > ${HOME}/service_key.json
  }

  function docker_login () {
    cat ${HOME}/service_key.json | docker login -u _json_key --password-stdin https://gcr.io
  }

  function docker_build () {
    docker pull gcr.io/$PROJECT_ID/$SERVICE_NAME:latest || true
    docker build --cache-from gcr.io/$PROJECT_ID/$SERVICE_NAME:latest -t gcr.io/$PROJECT_ID/$SERVICE_NAME:latest -t gcr.io/$PROJECT_ID/$SERVICE_NAME:$CI_COMMIT_SHA -f Dockerfile.$SERVICE_NAME .
  }

  function docker_push () {
    docker push gcr.io/$PROJECT_ID/$SERVICE_NAME:latest
  }

  function auth_google_cloud () {
    gcloud auth activate-service-account --key-file ${HOME}/service_key.json
  }

  function set_service_environment_name () {
    if [ $CI_ENVIRONMENT_SLUG = "production" ]; then
      SERVICE_ENVIRONMENT_NAME=$SERVICE_NAME
    else
      SERVICE_ENVIRONMENT_NAME=$CI_ENVIRONMENT_SLUG-$SERVICE_NAME
    fi
  }

  function create_cloud_run_service () {
    gcloud beta run deploy $SERVICE_ENVIRONMENT_NAME --region $REGION --project $PROJECT_ID --platform managed --image gcr.io/$PROJECT_ID/$SERVICE_NAME:latest
  }

  function make_cloud_run_externally_available () {
    gcloud beta run services add-iam-policy-binding $SERVICE_ENVIRONMENT_NAME --project $PROJECT_ID --member="allUsers" --role="roles/run.invoker" --region $REGION
  }

  function make_custom_domain () {
    if [[ $CI_ENVIRONMENT_SLUG == *"review"* ]]; then
      exit 0
    fi

    DOMAIN="${CI_ENVIRONMENT_URL#https://}"

    HAS_DOMAIN_ALREADY="$(gcloud beta run domain-mappings list --platform managed --project $PROJECT_ID --filter="spec.routeName=$SERVICE_ENVIRONMENT_NAME" || nothing_found)"
    if [[ $HAS_DOMAIN_ALREADY == *"nothing_found"* ]]; then
      gcloud beta run domain-mappings create --service $SERVICE_ENVIRONMENT_NAME --domain $DOMAIN --platform managed --project $PROJECT_ID --region $REGION
    fi

    echo "      ___                                       ___     "
    echo "     /\__\         _____          ___          /\  \    "
    echo "    /:/ _/_       /::\  \        /\  \        /::\  \   "
    echo "   /:/ /\  \     /:/\:\  \       \:\  \      /:/\:\__\  "
    echo "  /:/ /::\  \   /:/ /::\__\       \:\  \    /:/ /:/  /  "
    echo " /:/_/:/\:\__\ /:/_/:/\:|__|  ___  \:\__\  /:/_/:/__/___"
    echo " \:\/:/ /:/  / \:\/:/ /:/  / /\  \ |:|  |  \:\/:::::/  /"
    echo "  \::/ /:/  /   \::/_/:/  /  \:\  \|:|  |   \::/~~/~~~~ "
    echo "   \/_/:/  /     \:\/:/  /    \:\__|:|__|    \:\~~\     "
    echo "     /:/  /       \::/  /      \::::/__/      \:\__\    "
    echo "     \/__/         \/__/        ~~~~           \/__/    "
    echo ""
    echo ""
    echo "If you have remembered to set the DNS in cloudflare everything should be good to go"
    echo "You might need to wait a little for Cloud Run to provision a certificate."
    echo $DOMAIN
    echo ""
  }

  function delete_cloud_run_service () {
    gcloud beta run services delete $SERVICE_ENVIRONMENT_NAME --region $REGION --project $PROJECT_ID --platform managed -q
  }

before_script:
  - *common_functions
